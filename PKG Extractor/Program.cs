﻿using System;
using System.IO;
using System.Reflection;

namespace PKG_Extractor
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Missing parameters.");

                Console.WriteLine("Usage: " + Assembly.GetExecutingAssembly().GetName().Name + " pkg_filename [file1 file2]");

                return;
            }

            if (args.Length == 1)
                ExtractPKGFile(args[0]);

            if (args.Length > 1)
                ExtractPKGFile(args);
        }

        private static void ExtractPKGFile(string pkgFilename)
        {
            var extractor = new PKGExtractor();
            var header = new PKGHeader(pkgFilename);

            if (!header.IsMagicValid || !header.IsFinalizedValid || !header.IsTypeValid)
            {
                Console.WriteLine("ERROR: Unsupported PKG file.");

                return;
            }

            string path = Path.Combine(Path.GetDirectoryName(pkgFilename), header.TitleID);

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            extractor.ExtractFiles(pkgFilename, path);
        }

        private static void ExtractPKGFile(string[] args)
        {
            string pkgFilename = args[0];

            string[] requestedFiles = new string[args.Length - 1];

            Array.Copy(args, 1, requestedFiles, 0, requestedFiles.Length);

            var extractor = new PKGExtractor();
            var header = new PKGHeader(pkgFilename);

            if (!header.IsMagicValid || !header.IsFinalizedValid || !header.IsTypeValid)
            {
                Console.WriteLine("ERROR: Unsupported PKG file.");

                return;
            }

            string path = Path.Combine(Path.GetDirectoryName(pkgFilename), header.TitleID);

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            extractor.ExtractFiles(args[0], path, requestedFiles);
        }
    }
}
