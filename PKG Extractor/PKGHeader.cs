﻿using Extra.Utilities;
using System;
using System.IO;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;

namespace PKG_Extractor
{
    internal class PKGHeader
    {
        #region Constructors

        /// <summary>
        /// Decrypts a PS3 PKG file's header.
        /// </summary>
        /// <param name="encryptedPkg">The encrypted PS3 PKG file.</param>
        internal PKGHeader(string encryptedPkg)
        {
            using (var readStream = new FileStream(encryptedPkg, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.RandomAccess))
            using (var reader = new BinaryReader(readStream))
            {
                SetIsMagicValid(reader.ReadBytes(4)); // 0x00 (4 bytes) Contains the magic (.PKG)

                SetIsFinalized(reader.ReadByte()); // 0x04 (1 byte) Contains the finalized byte

                readStream.Position += 2;
                byte pkgType = reader.ReadByte();

                SetIsTypeValid(pkgType); // 0x07 (1 byte) Contains the PKG Type (0x01 for PS3)

                if (!IsMagicValid || !IsFinalizedValid || !IsTypeValid)
                    return;

                readStream.Position = 0x14; // 0x14 (4 bytes) Contains the file/folder count
                SetEntryCount(reader.ReadBytes(4));

                fileTableSize = entryCount * 0x20;

                SetFileSize(reader.ReadBytes(8)); // 0x18 (8 bytes) Contains the total size of the PKG file

                readStream.Position = 0x20; // 0x20 (8 bytes) Contains the starting offset for the encrypted data
                SetEncryptedDataOffset(reader.ReadBytes(8));

                SetEncryptedDataSize(reader.ReadBytes(8)); // 0x28 (8 bytes) Contains the size of the encrypted data

                SetContentID(reader.ReadBytes(0x24)); // 0x30 (0x24 bytes) Contains the CID

                if (isFinalized)
                {
                    readStream.Position = 0x70;

                    rivKey = reader.ReadBytes(0x10); // 0x70 (0x10 bytes) Contains the PKG file RIV key.
                }
                else
                {
                    readStream.Position = 0x60;

                    SetDebugKey(reader.ReadBytes(0x10)); // 0x60 (0x10 bytes) Contains the PKG file debug key parts 1 and 2 (8 bytes each)
                }

                readStream.Position = 0xE4;
                SetPKGKey(pkgType, reader.ReadInt32());

                if (!isFinalized)
                {
                    readStream.Position = encryptedDataOffset;
                    SetFileTableSize(reader.ReadBytes(0x10));
                }

                readStream.Position = encryptedDataOffset;
                SetFileTable(reader); // Decrypt the file table
            }
        }

        internal PKGHeader(byte[] rawPKGHeader)
        {
            using (var readStream = new MemoryStream(rawPKGHeader))
            using (var reader = new BinaryReader(readStream))
            {
                SetIsMagicValid(reader.ReadBytes(4)); // 0x00 (4 bytes) Contains the magic (.PKG)

                SetIsFinalized(reader.ReadByte()); // 0x04 (1 byte) Contains the finalized byte

                readStream.Position += 2;
                byte pkgType = reader.ReadByte();

                SetIsTypeValid(pkgType); // 0x07 (1 byte) Contains the PKG Type (0x01 for PS3)

                if (!IsMagicValid || !IsFinalizedValid || !IsTypeValid)
                    return;

                readStream.Position = 0x14; // 0x14 (4 bytes) Contains the file/folder count
                SetEntryCount(reader.ReadBytes(4));

                fileTableSize = entryCount * 0x20;

                SetFileSize(reader.ReadBytes(8)); // 0x18 (8 bytes) Contains the total size of the PKG file

                SetEncryptedDataOffset(reader.ReadBytes(8)); // 0x20 (8 bytes) Contains the starting offset for the encrypted data

                SetEncryptedDataSize(reader.ReadBytes(8)); // 0x28 (8 bytes) Contains the size of the encrypted data

                SetContentID(reader.ReadBytes(0x24)); // 0x30 (0x24 bytes) Contains the CID

                if (isFinalized)
                {
                    readStream.Position = 0x70;

                    rivKey = reader.ReadBytes(0x10); // 0x70 (0x10 bytes) Contains the PKG file RIV key.

                    readStream.Position = 0xE4;

                    SetPKGKey(pkgType, reader.ReadInt32());
                }
                else
                {
                    readStream.Position = 0x60;

                    SetDebugKey(reader.ReadBytes(0x10)); // 0x60 (0x10 bytes) Contains the PKG file debug key parts 1 and 2 (8 bytes each)
                }

                readStream.Position = encryptedDataOffset;
                SetFileTable(reader); // Decrypt the file table
            }
        }

        #endregion Constructors

        #region Fields

        internal static readonly byte[] PS3AesKey = { 0x2E, 0x7B, 0x71, 0xD7, 0xC9, 0xC9, 0xA1, 0x4E, 0xA3, 0x22, 0x1F, 0x18, 0x88, 0x28, 0xB8, 0xF8 };

        internal static readonly byte[] PSPAesKey = { 0x07, 0xF2, 0xC6, 0x82, 0x90, 0xB5, 0x0D, 0x2C, 0x33, 0x81, 0x8D, 0x70, 0x9B, 0x60, 0xE6, 0x2B };

        internal static readonly byte[] PSVAesKey2 = { 0xE3, 0x1A, 0x70, 0xC9, 0xCE, 0x1D, 0xD7, 0x2B, 0xF3, 0xC0, 0x62, 0x29, 0x63, 0xF2, 0xEC, 0xCB };

        internal static readonly byte[] PSVAesKey3 = { 0x42, 0x3A, 0xCA, 0x3A, 0x2B, 0xD5, 0x64, 0x9F, 0x96, 0x86, 0xAB, 0xAD, 0x6F, 0xD8, 0x80, 0x1F };

        internal static readonly byte[] PSVAesKey4 = { 0xAF, 0x07, 0xFD, 0x59, 0x65, 0x25, 0x27, 0xBA, 0xF1, 0x33, 0x89, 0x66, 0x8B, 0x17, 0xD9, 0xEA };

        internal byte[] aesKey;

        private string contentID;
        private long encryptedDataOffset, encryptedDataSize, pkgFileSize;
        private PKGEntry[] entries;

        private int entryCount, fileTableSize, keyType;
        private byte[] fileTable, debugKey, rivKey;

        private bool isMagicValid, isFinalized, isFinalizedValid, isTypeValid;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the PKG's ContentID.
        /// </summary>
        internal string ContentID { get { return contentID; } }

        /// <summary>
        /// Gets the PKG's debug key for debug PKGs.
        /// </summary>
        internal byte[] DebugKey { get { return debugKey; } }

        /// <summary>
        /// Gets the PKG's encrypted data's offset.
        /// </summary>
        internal long EncryptedDataOffset { get { return encryptedDataOffset; } }

        /// <summary>
        /// Gets the PKG's encrypted data's size.
        /// </summary>
        internal long EncryptedDataSize { get { return encryptedDataSize; } }

        /// <summary>
        /// Gets the PKG's entries.
        /// </summary>
        internal PKGEntry[] Entries { get { return entries; } }

        /// <summary>
        /// Gets the PKG's file count.
        /// </summary>
        internal int EntryCount { get { return entryCount; } }

        /// <summary>
        /// Gets the PKG's raw file table.
        /// </summary>
        internal byte[] FileTable { get { return fileTable; } }

        /// <summary>
        /// Gets the PKG's file table's size.
        /// </summary>
        internal int FileTableSize { get { return fileTableSize; } }

        /// <summary>
        /// Gets a boolean stating whether the PKG has been finalized.
        /// </summary>
        internal bool IsFinalized { get { return isFinalized; } }

        internal bool IsFinalizedValid { get { return isFinalizedValid; } }

        /// <summary>
        /// Gets a boolean stating whether the PKG's magic is valid.
        /// </summary>
        internal bool IsMagicValid { get { return isMagicValid; } }

        internal bool IsTypeValid { get { return isTypeValid; } }
        internal int KeyType { get { return keyType; } }

        /// <summary>
        /// Gets the PKG's file size.
        /// </summary>
        internal long PKGFileSize { get { return pkgFileSize; } }

        /// <summary>
        /// Gets the PKG's RIV key for retail PKGs.
        /// </summary>
        internal byte[] RIVKey { get { return rivKey; } }

        /// <summary>
        /// Gets the PKG's TitleID as extracted from the ContentID.
        /// </summary>
        internal string TitleID { get { return contentID.Substring(7, 9); } }

        #endregion Properties

        #region Private Methods

        private byte[] DecryptFilenameRetail(byte[] encryptedFilename, PKGEntry entry)
        {
            using (var ms = new MemoryStream())
            using (var bw = new BinaryWriter(ms))
            {
                byte[] pkgRIVKeyIncrement = new byte[rivKey.Length];

                Buffer.BlockCopy(rivKey, 0, pkgRIVKeyIncrement, 0, pkgRIVKeyIncrement.Length);

                Array.Reverse(pkgRIVKeyIncrement);

                var pkgRIVKeyIncrementRaw = new BigInteger(pkgRIVKeyIncrement);

                pkgRIVKeyIncrementRaw += (entry.FilenameOffset / pkgRIVKeyIncrement.Length);

                pkgRIVKeyIncrement = pkgRIVKeyIncrementRaw.ToByteArray();

                Array.Reverse(pkgRIVKeyIncrement);

                if (pkgRIVKeyIncrement.Length != rivKey.Length)
                {
                    byte[] tmpArr = new byte[rivKey.Length];

                    Buffer.BlockCopy(pkgRIVKeyIncrement, 0, tmpArr, 1, pkgRIVKeyIncrement.Length);

                    if (pkgRIVKeyIncrementRaw.Sign == 1)
                        tmpArr[0] = 0x00;
                    else if (pkgRIVKeyIncrementRaw.Sign == -1)
                        tmpArr[0] = 0xFF;

                    pkgRIVKeyIncrement = tmpArr;
                }

                for (int i = 0; i < encryptedFilename.Length; i += pkgRIVKeyIncrement.Length)
                {
                    byte[] encryptedData = new byte[pkgRIVKeyIncrement.Length];

                    Buffer.BlockCopy(encryptedFilename, i, encryptedData, 0, encryptedData.Length);

                    byte[] rivKeyConsecutive = new byte[pkgRIVKeyIncrement.Length];

                    Buffer.BlockCopy(pkgRIVKeyIncrement, 0, rivKeyConsecutive, 0, pkgRIVKeyIncrement.Length);

                    CryptographicEngines.IncrementArrayAtIndex(ref pkgRIVKeyIncrement, pkgRIVKeyIncrement.Length - 1);

                    byte[] pkgXorKeyConsecutive = null;

                    if (aesKey == PS3AesKey)
                        pkgXorKeyConsecutive = CryptographicEngines.Encrypt(rivKeyConsecutive, PS3AesKey, PS3AesKey, CipherMode.ECB, PaddingMode.None);
                    else
                    {
                        switch (keyType)
                        {
                            case 1:
                                if (entry.ContentType == 0 || entry.ContentType == 1 || entry.ContentType == 4)
                                    pkgXorKeyConsecutive = CryptographicEngines.Encrypt(rivKeyConsecutive, PS3AesKey, PS3AesKey, CipherMode.ECB, PaddingMode.None);
                                else if (entry.ContentType == 2)
                                    pkgXorKeyConsecutive = CryptographicEngines.Encrypt(rivKeyConsecutive, PSPAesKey, null, CipherMode.ECB, PaddingMode.None);
                                break;

                            case 2:
                            case 3:
                            case 4:
                                pkgXorKeyConsecutive = CryptographicEngines.Encrypt(rivKeyConsecutive, aesKey, aesKey, CipherMode.ECB, PaddingMode.None);
                                break;
                        }
                    }

                    byte[] decryptedData = CryptographicEngines.XOR(encryptedData, pkgXorKeyConsecutive);

                    bw.Write(decryptedData);
                }

                return ms.ToArray();
            }
        }

        /// <summary>
        /// Fills all PKG_Entry objects with their appropriate data
        /// </summary>
        private void PopulateEntries(BinaryReader reader)
        {
            using (var ms = new MemoryStream(fileTable))
            using (var br = new BinaryReader(ms))
                for (int i = 0; i < entryCount; i++)
                {
                    ms.Position = i * 0x20;

                    entries[i] = new PKGEntry(br.ReadBytes(0x20));

                    if (!isFinalized)
                    {
                        ms.Position = entries[i].FilenameOffset;
                        entries[i].SetFilename(br.ReadBytes(entries[i].FilenameSize));
                    }
                }

            if (!isFinalized)
                return;

            int lastOffset = (int)(encryptedDataOffset + entries[0].FileContentsOffset);

            reader.BaseStream.Position = encryptedDataOffset + fileTableSize;

            byte[] encryptedFilenamesTable = reader.ReadBytes(lastOffset - fileTableSize);

            if (encryptedFilenamesTable.Length == 0)
                return;

            for (int i = 0; i < entryCount; i++)
            {
                int alignment = 0x10 * ((entries[i].FilenameSize + 0xF) >> 4);

                byte[] encryptedFilename = new byte[alignment];

                Buffer.BlockCopy(encryptedFilenamesTable, entries[i].FilenameOffset - fileTableSize, encryptedFilename, 0, encryptedFilename.Length);

                byte[] filename = DecryptFilenameRetail(encryptedFilename, entries[i]);

                entries[i].SetFilename(filename);
            }
        }

        /// <summary>
        /// Sets the ContentID.
        /// </summary>
        /// <param name="value">0x24 bytes containing the ContentID.</param>
        private void SetContentID(byte[] value)
        {
            contentID = Encoding.UTF8.GetString(value);
        }

        /// <summary>
        /// Sets the debug PKG key for debug PKGs.
        /// </summary>
        /// <param name="value">0x10 bytes containing the debug key 1 and debug key 2 (8 bytes each).</param>
        private void SetDebugKey(byte[] value)
        {
            byte[] debugKey = new byte[value.Length * 4];
            byte[] tmpHolder = new byte[4];

            Buffer.BlockCopy(value, 0, debugKey, 0, value.Length / 2);
            Buffer.BlockCopy(value, 0, debugKey, value.Length / 2, value.Length / 2);
            Buffer.BlockCopy(value, value.Length / 2, debugKey, value.Length, value.Length / 2);
            Buffer.BlockCopy(value, value.Length / 2, debugKey, value.Length + (value.Length / 2), value.Length / 2);
            Buffer.BlockCopy(tmpHolder, 0, debugKey, debugKey.Length - tmpHolder.Length, tmpHolder.Length);

            this.debugKey = debugKey;
        }

        /// <summary>
        /// Sets the encrypted data's offset.
        /// </summary>
        /// <param name="value">8 bytes containing the encrypted data's offset.</param>
        private void SetEncryptedDataOffset(byte[] value)
        {
            MiscUtils.CheckEndianness(ref value);

            encryptedDataOffset = BitConverter.ToInt64(value, 0);
        }

        /// <summary>
        /// Sets the encrypted data's size.
        /// </summary>
        /// <param name="value">8 bytes containing the encrypted data's size.</param>
        private void SetEncryptedDataSize(byte[] value)
        {
            MiscUtils.CheckEndianness(ref value);

            encryptedDataSize = BitConverter.ToInt64(value, 0);
        }

        /// <summary>
        /// Sets the file count and initializes the required number of entries.
        /// </summary>
        /// <param name="value">4 bytes containing the file count.</param>
        private void SetEntryCount(byte[] value)
        {
            MiscUtils.CheckEndianness(ref value);

            entryCount = BitConverter.ToInt32(value, 0);

            entries = new PKGEntry[entryCount];
        }

        /// <summary>
        /// Sets the PKG's file size.
        /// </summary>
        /// <param name="value">8 bytes containing the PKG size.</param>
        private void SetFileSize(byte[] value)
        {
            MiscUtils.CheckEndianness(ref value);

            pkgFileSize = BitConverter.ToInt64(value, 0);
        }

        /// <summary>
        /// Sets the file table, which contains the raw entries information.
        /// </summary>
        /// <param name="reader">Currently open reader stream to the PKG file.</param>
        /// <param name="aesKey">AES key</param>
        private void SetFileTable(BinaryReader reader)
        {
            using (var ms = new MemoryStream())
            using (var bw = new BinaryWriter(ms))
            {
                if (isFinalized)
                {
                    byte[] pkgRIVKeyIncrement = new byte[rivKey.Length];

                    Buffer.BlockCopy(rivKey, 0, pkgRIVKeyIncrement, 0, pkgRIVKeyIncrement.Length);

                    while (reader.BaseStream.Position < (encryptedDataOffset + fileTableSize))
                    {
                        byte[] encryptedData = reader.ReadBytes(0x10);

                        byte[] rivKeyConsecutive = new byte[pkgRIVKeyIncrement.Length];

                        Buffer.BlockCopy(pkgRIVKeyIncrement, 0, rivKeyConsecutive, 0, pkgRIVKeyIncrement.Length);

                        CryptographicEngines.IncrementArrayAtIndex(ref pkgRIVKeyIncrement, pkgRIVKeyIncrement.Length - 1);

                        byte[] pkgXORKeyConsecutive = CryptographicEngines.Encrypt(rivKeyConsecutive, aesKey, aesKey, CipherMode.ECB, PaddingMode.None);

                        byte[] decryptedData = CryptographicEngines.XOR(encryptedData, pkgXORKeyConsecutive);

                        bw.Write(decryptedData);

                        if (reader.BaseStream.Position == reader.BaseStream.Length)
                            break;
                    }
                }
                else
                {
                    byte[] keyIncrement = new byte[debugKey.Length];

                    Buffer.BlockCopy(debugKey, 0, keyIncrement, 0, keyIncrement.Length);

                    while (reader.BaseStream.Position < (encryptedDataOffset + fileTableSize))
                    {
                        byte[] encryptedData = reader.ReadBytes(0x10);

                        byte[] keyHash;
                        using (SHA1Managed hasher = new SHA1Managed())
                            keyHash = hasher.ComputeHash(keyIncrement);

                        CryptographicEngines.IncrementArrayAtIndex(ref keyIncrement, keyIncrement.Length - 1);

                        byte[] decryptedData = CryptographicEngines.XOR(encryptedData, keyHash);

                        bw.Write(decryptedData);
                    }
                }

                fileTable = ms.ToArray();
            }

            PopulateEntries(reader);
        }

        private void SetFileTableSize(byte[] encryptedData)
        {
            byte[] pkgXorKey;

            if (isFinalized)
                // The riv key has to be encrypted with a global AES key to generate the XOR key. PSP uses CipherMode.ECB, PaddingMode.None and doesn't need IV
                pkgXorKey = CryptographicEngines.Encrypt(rivKey, aesKey, aesKey, CipherMode.ECB, PaddingMode.None);
            else
                using (SHA1Managed hasher = new SHA1Managed())
                    pkgXorKey = hasher.ComputeHash(debugKey);

            // XOR Decrypt
            byte[] decryptedData = CryptographicEngines.XOR(encryptedData, pkgXorKey);

            byte[] fileTableSize = new byte[sizeof(long)];

            Buffer.BlockCopy(decryptedData, 8, fileTableSize, 0, fileTableSize.Length);

            MiscUtils.CheckEndianness(ref fileTableSize);

            this.fileTableSize = (int)BitConverter.ToInt64(fileTableSize, 0);
        }

        /// <summary>
        /// Sets whether the PKG has been finalized.
        /// </summary>
        /// <param name="value">1 byte containing the finalization value.</param>
        private void SetIsFinalized(byte value)
        {
            if (value == 0x80)
            {
                isFinalized = true;
                isFinalizedValid = true;
            }
            else if (value == 0)
            {
                isFinalized = false;
                isFinalizedValid = true;
            }
            else
                isFinalizedValid = false;
        }

        /// <summary>
        /// Sets whether the PKG's magic bytes are valid.
        /// </summary>
        /// <param name="value">4 bytes containing the magic.</param>
        private void SetIsMagicValid(byte[] value)
        {
            byte[] magic = new byte[] { 0x7F, 0x50, 0x4B, 0x47 };

            isMagicValid = CryptographicEngines.CompareBytes(value, 0, magic, 0, value.Length);
        }

        private void SetIsTypeValid(byte value)
        {
            isTypeValid = value == 1 || value == 2;
        }

        private void SetPKGKey(byte value, int keyType)
        {
            if (value == 1)
                aesKey = PS3AesKey;
            else
            {
                this.keyType = (keyType >> 24) & 7;

                switch (this.keyType)
                {
                    case 1:
                        aesKey = PSPAesKey;
                        break;

                    case 2:
                        aesKey = CryptographicEngines.Encrypt(RIVKey, PSVAesKey2, null, CipherMode.ECB, PaddingMode.None);
                        break;

                    case 3:
                        aesKey = CryptographicEngines.Encrypt(RIVKey, PSVAesKey3, null, CipherMode.ECB, PaddingMode.None);
                        break;

                    case 4:
                        aesKey = CryptographicEngines.Encrypt(RIVKey, PSVAesKey4, null, CipherMode.ECB, PaddingMode.None);
                        break;
                }
            }
        }
    }

    #endregion Private Methods
}
